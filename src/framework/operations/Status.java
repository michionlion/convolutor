package framework.operations;

public enum Status {

        UNIDENTIFIED_ERROR("Unidentified error!"), CANCELED_OPERATION("Operation was canceled!"), SUCCESSFUL_OPERATION("Successful operation!"),
        IOEXCEPTION_THROWN("Java I/O Exception was thrown!"), FILE_DOES_NOT_EXIST("The file does not exist!"), FILE_IS_NOT_READABLE("The file is not readable!"),
        FILE_IS_NOT_WRITABLE("The file cannot be written to!"), FILE_IS_NOT_FILE("The file is not actually a file!"), FILE_IS_NOT_CORRECT_FORMAT("The file is not in the correct format!"),
        OPERATION_DOES_NOT_EXIST("The operation does not exist!"), OPERATION_FORMAT_WRONG("The operation was formatted incorrectly!");
        String desc;

        private Status(String d) {
            desc = d;
        }

        public String getMsg() {
            return desc;
        }

        public boolean isError() {
            return this != SUCCESSFUL_OPERATION;
        }
    }
