package framework.operations;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.awt.image.ColorConvertOp;
import java.io.Serializable;
import java.util.ArrayList;
import windows.Display;

public abstract class OPS {

    public static final ArrayList<Op> Ops;

    static {
        Ops = new ArrayList<>();
        try { // load premade operations
            Ops.add(new ConvertToGrayscaleOp()); // add special convert to grayscale op
            Ops.add(IO.loadOp("gaussBlur")); // file names to load and test initially
            Ops.add(IO.loadOp("sobel"));
            Ops.add(IO.loadOp("boxBlur"));
            Ops.add(IO.loadOp("sharpen"));
            Ops.add(IO.loadOp("unsharpen"));
            Ops.add(IO.loadOp("simple-edge"));
            Ops.add(IO.loadOp("edge-x"));
            Ops.add(IO.loadOp("edge-y"));
        } catch (IO.OperationFormatWrong ex) {
            throw new Error("Built-in operations have been modified or corrupted: " + ex.getMessage());
        }
    }

    private static BufferedImage currentImage = new BufferedImage(600, 350, BufferedImage.TYPE_INT_ARGB);
    private static String imageFileName = null; // filename from where currentImage was loaded
    private static boolean inProgress = false;

    public static Status executeOp(Op op) {
        inProgress = true;
        op.doOp();
        inProgress = false;
        Display.refreshDisplay();
        return Status.SUCCESSFUL_OPERATION;
    }

    public static Status executeOp(String name) {
        Op op = getOp(name);
        if (op == null) {
            return Status.OPERATION_DOES_NOT_EXIST;
        }
        return executeOp(op);
    }

    public static Op getOp(String name) { // returns Op that has name 'name' if exists, otherwise returns null
        for (Op op : Ops) {
            if (op.getName().equalsIgnoreCase(name)) {
                return op;
            }
        }
        Op op = null;
        try {
            op = IO.loadOp(name);
        } catch (IO.OperationFormatWrong ex) {
            //file doesn't exist or some other problem
        }
        if (op != null) {
            Ops.add(op);
        }
        return op; // returns null or an Op
    }

    // Dealing with changing the working image
    public static String getCurrentFileName() {
        return imageFileName;
    }

    public static BufferedImage getCurrentImage() {
        return currentImage;
    }

    public static void setCurrentImage(BufferedImage currentImage, String name) { // sets current image
        OPS.currentImage = currentImage;
        imageFileName = name;
        Display.refreshDisplay();
    }

    public static class Pixel { // convert between datatypes

        short r, g, b; //shorts hold 2 bytes instead of 4, as ints do. This enables overflow from taking byte values of colors to be rounded down accordingly

        public Pixel(int x, int y) { // convert specified coord in currentImage from int to argb
            Color c = new Color(currentImage.getRGB(x, y));

            r = (short) c.getRed();
            g = (short) c.getGreen();
            b = (short) c.getBlue();
        }

        public Pixel(int argb) { // convert int to argb
            Color c = new Color(argb);
            r = (short) c.getRed();
            g = (short) c.getGreen();
            b = (short) c.getBlue();

        }

        public Pixel(int r, int g, int b) { // NO CHECKING!
            this.r = (short) r;
            this.g = (short) g;
            this.b = (short) b;
        }

        public int[] argb() {
            int[] res = {r, g, b};
            return res;
        }

        // props to a user who answered this question on stackoverflow: http://stackoverflow.com/questions/4801366/convert-rgb-values-into-integer-pixel. I could have used Color.getRGB(), but wanted to go for something a bit faster
        public int toInt() {

            cut(); // cut values to boolean range

            //    alpha calc (always 255)  red         green          blue
            return (255 << 24) | (r << 16) | (g << 8) | b;
        }

        public void cut() {
            if (r < 0) {
                r = 0;
            } else if (r > 255) {
                r = 255;
            }
            if (g < 0) {
                g = 0;
            } else if (g > 255) {
                g = 255;
            }
            if (b < 0) {
                b = 0;
            } else if (b > 255) {
                b = 255;
            }

        }

        private void zero() {
            r = 0;
            g = 0;
            b = 0;
        }

        @Override
        public String toString() {
            return "RGB: " + r + ", " + g + ", " + b;
        }

        public static short[] toRGB(int argb) {
            Pixel p = new Pixel(argb);
            short[] res = {p.r, p.g, p.b};
            return res;
        }

        public static Pixel get(int x, int y) { // handle edges by 'extending' them, constraining x and y to edge pixel
            if (x < 0) {
                x = 0;
            } else if (x >= currentImage.getWidth()) {
                x = currentImage.getWidth() - 1;
            }
            if (y < 0) {
                y = 0;
            } else if (y >= currentImage.getHeight()) {
                y = currentImage.getHeight() - 1;
            }
            Pixel res = new Pixel(x, y);
            //System.out.println("cut to getting pixel at " + x + ", " + y + ", which is " + res.a + ", " + res.r + ", " + res.g + ", " + res.b);
            return res;
        }
    }

    // op classes, all act on currentImage
    public static abstract class Op implements Serializable { // serializable not needed currently, but why not?

        private final String name;

        public Op(String opName) {
            name = opName;
        }

        public abstract void doOp();

        public String getName() {
            return name;
        }
    }

    public static class MatrixOp extends Op {

        protected final float[][] matrix;
        protected final int width, height;

        public MatrixOp(String name, float[][] matrix) {
            super(name);
            this.matrix = matrix;
            width = matrix.length;
            height = matrix[0].length; // making assumption that matrix has atleast one column, which really MUST be true, otherwise loadOp would throw an exception
        }

        @Override
        public void doOp() {
            //apply matrix to currentImage
            BufferedImage output = new BufferedImage(currentImage.getWidth(), currentImage.getHeight(), BufferedImage.TYPE_INT_ARGB); // image to modify

            float matrixVal;
            int mtx, mty; // don't want to declare variables inside quadruaple for-loops.  That might be a lot of variables, so just reuse
            Pixel p, sum = new Pixel(0, 0, 0);
            for (int x = 0; x < currentImage.getWidth(); x++) { // for every pixel
                for (int y = 0; y < currentImage.getHeight(); y++) {

                    sum.zero();

                    //System.out.println(x + ", " + y);
                    mty = 0;
                    //apply matrix to pixel at x,yint my = -height/2; my <= height/2; my++
                    for (int my = -height / 2; my <= height / 2; my++) { // loop through matrix with mx and my as offsets from center slot, and mtx,mty as matrix coords for current calc
                        mtx = 0;
                        for (int mx = -width / 2; mx <= width / 2; mx++) {
                            p = Pixel.get(x + mx, y + my);
                            matrixVal = matrix[mtx][mty];
                            sum.r += p.r * matrixVal;
                            sum.g += p.g * matrixVal;
                            sum.b += p.b * matrixVal;

                            mtx++;
                        }
                        mty++;
                    }
                    //System.out.println("calced "+x+", "+y+" " + sum);
                    // set pixel at x,y to sums of matrix op
                    output.setRGB(x, y, sum.toInt());
                }
            }

            currentImage = output; // flip image to output
        }
    }

    public static class CombinationOp extends Op { // can combine combinationOps and matrix ops to create complex convolution

        protected final Op[] orderedOps;

        public CombinationOp(String name, Op... ops) {
            super(name);
            orderedOps = ops;
        }

        @Override
        public void doOp() {
            for (Op o : orderedOps) {
                o.doOp();
            }
        }
    }

    public static class SimultaneousCombinationOp extends CombinationOp {

        protected BufferedImage toApplyOn;
        protected BufferedImage[] results;
        protected CombineMethod method;

        public SimultaneousCombinationOp(String name, CombineMethod method, Op... ops) {
            super(name, ops);
            this.method = method==null ? new CombineMethod() : method; // if no method specified, set to default
            results = new BufferedImage[orderedOps.length];
        }

        @Override
        public void doOp() {
            toApplyOn = currentImage;

            for (int i = 0; i < orderedOps.length; i++) {
                Op op = orderedOps[i];
                op.doOp();
                results[i] = currentImage; // store the result of the op
                currentImage = toApplyOn; // reset the image
            }

            //combine images
            BufferedImage output = new BufferedImage(currentImage.getWidth(), currentImage.getHeight(), BufferedImage.TYPE_INT_ARGB);

            Pixel sum = new Pixel(0, 0);
            Pixel p;
            for (int x = 0; x < currentImage.getWidth(); x++) { // for every pixel
                for (int y = 0; y < currentImage.getHeight(); y++) {
                    sum.zero();
                    for (BufferedImage img : results) {
                        p = new Pixel(img.getRGB(x, y));
                        sum.r += method.addMod(p.r); // apply add mod
                        sum.g += method.addMod(p.g);
                        sum.b += method.addMod(p.b);
                    }

                    sum = method.normMod(sum); // apply normalization mod
                    
                    output.setRGB(x, y, sum.toInt());
                }
            }

            currentImage = output; // and... flip to output

        }
    }

    public static final class ConvertToGrayscaleOp extends Op {

        public ConvertToGrayscaleOp(String parentName) {
            super(parentName + "-grayscale");
        }

        public ConvertToGrayscaleOp() {
            super("grayscale");
        }

        @Override
        public void doOp() {
            BufferedImage image = new BufferedImage(currentImage.getWidth(), currentImage.getHeight(), BufferedImage.TYPE_BYTE_GRAY);
            ColorConvertOp op = new ColorConvertOp(currentImage.getColorModel().getColorSpace(), image.getColorModel().getColorSpace(), null);
            op.filter(currentImage, image); // converted to grayscale

            //do not want to draw on currentImage, so create placeholder and flip after complete
            BufferedImage res = new BufferedImage(currentImage.getWidth(), currentImage.getHeight(), BufferedImage.TYPE_INT_ARGB);
            res.getGraphics().drawImage(image, 0, 0, null);
            currentImage = res;
        }
    }
    
    public static class CombineMethod {
            public int addMod(int pixToAdd) {
                return pixToAdd;
            }
            
            public Pixel normMod(Pixel p) {
                return p;
            }
        }
        
        public static class SobelMethod extends CombineMethod {
            @Override
            public int addMod(int pixToAdd) {
                return pixToAdd*pixToAdd;
            }
            
            @Override
            public Pixel normMod(Pixel p) {
                p.r = (short) Math.round(Math.sqrt(p.r)); // all of the outputs should be smaller than the inputs, so casting will not change anything
                p.g = (short) Math.round(Math.sqrt(p.g));
                p.b = (short) Math.round(Math.sqrt(p.b));
                return p;
            }
        }
}