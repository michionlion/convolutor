package framework.operations;

import static framework.operations.Status.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Scanner;
import javax.imageio.ImageIO;
import javax.swing.JFileChooser;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;
import windows.Terminal;

public abstract class IO {

    private static final JFileChooser fileChooser = new JFileChooser("."); // filechooser with run directory as default
    private static File lastUsed = new File("start.png");
    public static final FileFilter OP_FILTER = new FileNameExtensionFilter("OP Files", "OP");
    public static final FileFilter IMG_FILTER = new FileNameExtensionFilter("Image Files", "PNG", "JPG", "JPEG");

    public static OPS.Op loadOp(String name) throws OperationFormatWrong {
        File f = new File("operations" + File.separator + name + ".op"); // files are placed in 'operations' sub-folder, using File.separator to ensure cross-compatability

        if (!f.exists()) {
            throw new OperationFormatWrong(name, f.getName() + " does not exist!");
        }
        if (!f.isFile()) {
            throw new OperationFormatWrong(name, f.getName() + " is not a file!");
        }
        if (!f.canRead()) {
            throw new OperationFormatWrong(name, f.getName() + " is not readable!");
        }
        Scanner in;
        try {
            in = new Scanner(f);
        } catch (FileNotFoundException ex) {
            throw new Error("File not found, but got past !file.exists(). Something is seriously wrong!"); // uncaught exception, something is really, really wrong
        }

        lastUsed = f; // we know f could be useable, so parsing and setting lastUsed
        OPS.Op operation;

        String line = in.nextLine();
        String[] elements = line.split(";"); // Header is in format: 'name;type'
        if (elements.length < 2) {
            throw new OperationFormatWrong(name, "Header does not contain correct elements, should be 'name;type'");
        }
        if (elements[1].equalsIgnoreCase("matrix")) {
            //load matrixOp

            String n = elements[0]; // name of op

            line = in.nextLine(); // second line for matrix type is 'widthxheight'
            elements = line.split("x");
            int width, height;
            boolean norm = false;
            boolean grayscale = false;
            try {
                width = Integer.parseInt(elements[0]);
                height = Integer.parseInt(elements[1]);
            } catch (Exception ex) { // catches index out of bounds as well as parsing errors
                throw new OperationFormatWrong(name, "Failed to parse matrix size");
            }

            line = in.nextLine(); // third line is 'Normalized:boolean'
            try {
                elements = line.split(":");
                norm = elements[1].equalsIgnoreCase("true") || elements[1].equalsIgnoreCase("yes") || elements[1].equalsIgnoreCase("y"); // a few possible values to evaluate to true
            } catch (Exception ex) { // catches index out of bounds as well as parsing errors
                throw new OperationFormatWrong(name, "Failed to parse normalization value, must be either 'true', 'yes', or 'y' to evaluate to true, or anything else to evaluate to false");
            }

            line = in.nextLine(); // fourth line is 'Grayscale:boolean'
            try {
                elements = line.split(":");
                grayscale = elements[1].equalsIgnoreCase("true") || elements[1].equalsIgnoreCase("yes") || elements[1].equalsIgnoreCase("y"); // a few possible values to evaluate to true
            } catch (Exception ex) { // catches index out of bounds as well as parsing errors
                throw new OperationFormatWrong(name, "Failed to parse grayscale value, must be either 'true', 'yes', or 'y' to evaluate to true, or anything else to evaluate to false");
            }

            float[][] mtrx = new float[width][height];

            try {
                for (int i = 0; i < height; i++) { // for each row
                    for (int j = 0; j < width; j++) { // for each entry in row
                        mtrx[j][i] = in.nextFloat(); // values are split by spaces and newlines
                    }
                }
            } catch (Exception ex) {
                throw new OperationFormatWrong(n, "Failed to parse matrix values");
            }
            if (norm) { // do normalization
                float sum = 0;

                for (float[] vals : mtrx) { // get sum of all values
                    for (float val : vals) {
                        sum += val;
                    }
                }

                for (int i = 0; i < width; i++) { // do normalization calcs
                    for (int j = 0; j < height; j++) {
                        mtrx[i][j] /= sum;
                    }
                }
            }

            //if it is a grayscale operation, add a comboOp with a convert to grayscale op first, then matrixOp. Otherwise, just a matrixOp
            operation = grayscale ? new OPS.CombinationOp(n, new OPS.ConvertToGrayscaleOp(), new OPS.MatrixOp(n + "-matrix", mtrx)) : new OPS.MatrixOp(n, mtrx);

        } else if (elements[1].equalsIgnoreCase("combo")) {
            //load combinationOp

            boolean simul;
            line = in.nextLine(); // second line is 'Simultaneous:boolean'
            try {
                elements = line.split(":");
                simul = elements[1].equalsIgnoreCase("true") || elements[1].equalsIgnoreCase("yes") || elements[1].equalsIgnoreCase("y"); // a few possible values to evaluate to true
            } catch (Exception ex) { // catches index out of bounds as well as parsing errors
                throw new OperationFormatWrong(name, "Failed to parse simultaneous value, must be either 'true', 'yes', or 'y' to evaluate to true, or anything else to evaluate to false");
            }

            // from second line on, each line has a name of another .op file, corresponding to the next operation in the combo op.  
            ArrayList<String> lines = new ArrayList<>();

            String n = elements[0]; // name of op

            while (in.hasNext()) {
                lines.add(in.nextLine());
            }

            OPS.Op[] ops = new OPS.Op[lines.size()];

            for (int i = 0; i < lines.size(); i++) {
                line = lines.get(i);
                try {
                    ops[i] = loadOp(line); // recursive op creation, AWESOMELY SIMPLE
                } catch (OperationFormatWrong ex) { // catch to make sure top-level Operation eventually gets it's name in the front of error msg
                    throw new OperationFormatWrong(name, "recursive creation error: " + ex.getMessage());
                }
            }

            operation = simul ? new OPS.SimultaneousCombinationOp(n, n.contains("sobel") ? new OPS.SobelMethod() : null, ops) : new OPS.CombinationOp(n, ops); // hardcoding sobelMethod to simul op containing 'sobel' in name, could extend as an option with minimal effor later
        } else {
            throw new OperationFormatWrong(name, "Type specified not recognized! (must be 'matrix' or 'combo'");
        }

        return operation;
    }
    
    /**
     * saveOp expects the opContents to EXACTLY match what should be written to file, including newlines. The file name will be taken from the first line, up until a ';' is reached
     * @param opContents the contents to save as an op file
     * @return
     */
    public static Status saveOp(String opContents) { // don't actually have an implementation for directly saving an Op because we won't need it, this will only be used for custom Ops
        File file = new File("operations"+File.separator+opContents.split(";")[0]+".op");
        try {
            file.createNewFile();
            try (PrintWriter out = new PrintWriter(file)) { // a try with resources statement, which ensures that even if the try exits abnormally (ie, catch is called), the resources get closed
                out.print(opContents); // print contents
                
                
                out.flush(); // do writing
                
                if(out.checkError()) return UNIDENTIFIED_ERROR; // ensure writing went smoothly
                
            }
        }catch(Exception ex) {
            if(ex instanceof IOException) return IOEXCEPTION_THROWN;
        }
        
        return SUCCESSFUL_OPERATION;
    }

    public static Status save() { // save to last used file
        return save(lastUsed.getName());
    }

    public static Status save(String fileName) {
        if (fileName != null && fileName.contains(".PNG")) {
            fileName = fileName.replace(".PNG", ".png"); // ensure png is lowercase, don't need to do with other extensions cause we'll overwrite them
        }
        File target;

        if (fileName != null) {
            target = new File(fileName);

        } else {

            int res = fileChooser.showSaveDialog(Terminal.getFrameToChild());

            if (res == JFileChooser.APPROVE_OPTION) {
                target = fileChooser.getSelectedFile();
            } else {
                return CANCELED_OPERATION;
            }
        }

        //ensure extension is png for write op
        if (IMG_FILTER.accept(target) && !target.getName().endsWith(".png")) { // an extension other than png was specified
            String name = target.getName().substring(0, target.getName().lastIndexOf(".")) + ".png"; // index is not -1 because passed accept(), so must have an extension
            target = new File(name);
        } else if (!IMG_FILTER.accept(target)) {
            target = new File(target.getName() + ".png");
        }

        if (!target.exists()) {
            try {
                target.createNewFile();
            } catch (Exception ex) {
                return IOEXCEPTION_THROWN;
            }
        } else if (!target.canWrite()) {
            return FILE_IS_NOT_WRITABLE;
        }

        try {
            lastUsed = target;
            boolean b = ImageIO.write(OPS.getCurrentImage(), "PNG", target);
            if (!b) {
                return UNIDENTIFIED_ERROR;
            }
            return SUCCESSFUL_OPERATION;
        } catch (Exception ex) {
            return IOEXCEPTION_THROWN;
        }
    }

    public static Status loadImage(String fileName) { // fileName INCLUDES .png or extension
        File img;

        if (fileName == null) { // get file by fileChooser

            fileChooser.setFileFilter(IMG_FILTER);

            int res = fileChooser.showOpenDialog(Terminal.getFrameToChild());

            if (res == JFileChooser.APPROVE_OPTION) {
                img = fileChooser.getSelectedFile();
            } else {
                return CANCELED_OPERATION;
            }

        } else { // fileName specifies file
            img = new File(fileName);
        }

        // ERROR CHECKING
        if (!img.exists()) {
            return FILE_DOES_NOT_EXIST;
        }
        if (!img.isFile()) {
            return FILE_IS_NOT_FILE;
        }
        if (!img.canRead()) {
            return FILE_IS_NOT_READABLE;
        }
        if (!IMG_FILTER.accept(img)) { // if filter doesn't accept img, it is in wrong format
            return FILE_IS_NOT_CORRECT_FORMAT;
        }

        BufferedImage loaded;

        try {
            loaded = convertToARGB(ImageIO.read(img)); // load image and convert to correct format
        } catch (IOException ex) {
            return IOEXCEPTION_THROWN;
        }

        if (loaded != null) {
            lastUsed = img;
            OPS.setCurrentImage(loaded, img.getName());
        } else {
            return UNIDENTIFIED_ERROR;
        }

        return SUCCESSFUL_OPERATION;

    }

    public static File getRecentModifiedFile() {
        return lastUsed;
    }

    public static class OperationFormatWrong extends IOException { // just a fancy name for an IOException to throw when load ops do not go as planned

        public OperationFormatWrong(String sourceOpFileName, String msg) {
            super("Unable to create " + sourceOpFileName + ", error: " + msg);
        }

        public OperationFormatWrong() {
            super("Reason not specified!");
        }
    }

    public static BufferedImage convertToARGB(BufferedImage img) {
        BufferedImage res = new BufferedImage(img.getWidth(), img.getHeight(), BufferedImage.TYPE_INT_ARGB);
        res.getGraphics().drawImage(img, 0, 0, null); // just draw the image onto another of the right type to convert
        return res;
    }
}
