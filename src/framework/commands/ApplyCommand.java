package framework.commands;

import framework.operations.OPS;
import framework.operations.Status;

public class ApplyCommand extends Command {
    
    protected ApplyCommand() {
        
    }
    
    @Override
    public boolean isAlias(String s) {
        return s.equals("a");
    }
    
    @Override
    public String getCommand() {
        return "apply";
    }

    @Override
    public String invoke(String... args) {
        if(args.length < 2) return "No operation specified!";
        StringBuilder applied = new StringBuilder();
        StringBuilder failed = new StringBuilder();
        for(int i=1; i<args.length; i++) { // starting at 1 to avoid 'apply' command
            Status outcome = OPS.executeOp(args[i]);
            if(outcome==Status.SUCCESSFUL_OPERATION) applied.append(applied.length()==0 ? "" : ", ").append(args[i]); // append ', name' to applied unless it is the first one
            else failed.append(failed.length()==0 ? "" : "\n").append("Operation '").append(args[i]).append("' failed: ").append(outcome.getMsg());
        }
        
        return (applied.length()==0 ? "Failed to apply!" : ("Applied " + applied)) + (failed.length()==0 ? "" : "\n"+failed.toString());
    }

    @Override
    public String helpString() {
        return "Applies an operation or series of operations to the current working image\nUseage:\napply <operationName>\napply <operationName1> <operationName2> ...";
    }
    
}
