package framework.commands;

import framework.operations.IO;
import framework.operations.OPS;
import framework.operations.Status;
import windows.Display;

public class UseCommand extends Command {

    @Override
    public String getCommand() {
        return "use";
    }
    
    @Override
    public boolean isAlias(String s) {
        return s.equalsIgnoreCase("load") || s.equalsIgnoreCase("u") || s.equalsIgnoreCase("l");
    }

    @Override
    public String invoke(String... args) {
        Status outcome;
        if(args.length < 2) {
            outcome = IO.loadImage(null);
        } else {
            outcome = IO.loadImage(args[1]); // loads and displays image, returns outcome
        }
        Display.refreshDisplay(); //for some reason this works better than having the call somewhere in loadImage() or it's called methods
        return !outcome.isError() ? "Loaded "+IO.getRecentModifiedFile().getName() : "ERROR: "+ outcome.getMsg();
    }

    @Override
    public String helpString() {
        return "Loads an image and then displays it, ready for manipulation\nUseage:\nuse\nuse <fileName.png>";
    }
    
}
