package framework.commands;

import framework.operations.IO;
import framework.operations.Status;
import windows.CustomOpCreator;
import windows.Terminal;

public class CreateCommand extends Command {

    @Override
    public boolean isAlias(String s) {
        return s.equalsIgnoreCase("c");
    }
    
    @Override
    public String getCommand() {
        return "create";
    }

    @Override
    public String invoke(String... args) {
        
        Status outcome = CustomOpCreator.getOp(Terminal.getFrameToChild());
        
        return !outcome.isError() ? "Created "+ IO.getRecentModifiedFile().getName() : "ERROR: "+ outcome.getMsg();
    }

    @Override
    public String helpString() {
        return "Displays a GUI for creation of custom operations\nUseage:\ncreate";
    }
    
}
