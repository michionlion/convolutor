package framework.commands;

import framework.operations.IO;
import java.io.File;
import java.io.FileFilter;

public class ListCommand extends Command {

    protected ListCommand() {
        
    }
    
    @Override
    public String getCommand() {
        return "list";
    }
    
    @Override
    public boolean isAlias(String s) {
        return s.equalsIgnoreCase("ls");
    }

    @Override
    public String invoke(String... args) {
        File f;
        if(args.length > 1) f = new File(args[1]);
        else f = new File(".");
        
        //File[] ls = f.listFiles(file -> file.getName().endsWith(".png") || file.getName().endsWith(".op")); // lists files ending with .png or .op.  ALSO, LAMBDAS ARE AWESOME
        File[] ls = f.listFiles(new FileFilter() {
            @Override
            public boolean accept(File pathname) {
                return IO.IMG_FILTER.accept(pathname) && pathname.isFile(); // accept files which pass useability filters
                
            }
        }); // lists files ending with .png or .op.  ALSO, LAMBDAS ARE AWESOME
        
        if(ls==null || ls.length==0) return "No usable files in " + (args.length>1 ? args[1] : "main directory");
        
        StringBuilder out = new StringBuilder();
        out.append("Usable files in ").append(args.length>1 ? args[1] : "main directory").append(":");
        for (File l : ls) {
            out.append("\n|  ").append(l.getName());
            
        }
        
        return out.toString();
    }

    @Override
    public String helpString() {
        return "Lists the files in the main directory\nUseage:\nlist\nlist <dir>";
    }
    
}
