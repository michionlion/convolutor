package framework.commands;

import java.util.ArrayList;


public abstract class Command {
    
    protected static ArrayList<Command> cmds = new ArrayList<>();
    static {
        // add each command once here, empty command first
        cmds.add(new EmptyCommand());
        cmds.add(new HelpCommand());
        cmds.add(new UseCommand());
        cmds.add(new ListCommand());
        cmds.add(new SaveCommand());
        cmds.add(new ApplyCommand());
        cmds.add(new CreateCommand());
    }
    
    public static Command getCommandFromString(String s) {
        for(Command c : cmds) {
            if(c.getCommand().equalsIgnoreCase(s.trim())) return c;
            if(c.isAlias(s.trim())) return c;
        }
        return cmds.get(0); // return empty command
    }
    
    public boolean isAlias(String s) {
        return false;
    }
    
    public abstract String getCommand();
    public abstract String invoke(String... args); // args contains in slot 0 the command, slot 1 the first argument, etc
    public abstract String helpString();
    
    static class EmptyCommand extends Command {
        
        private EmptyCommand() { // protect against initilization outside of package
            
        }

        @Override
        public String getCommand() {
            return "";
        }
        
        @Override
        public boolean isAlias(String s) {
        return s == null || s.equalsIgnoreCase(" ");
    }

        @Override
        public String invoke(String[] args) {
            return args[0] + " is not a command!";
        }

        @Override
        public String helpString() {
            return "An empty or unrecognized command";
        }
        
    }
}
