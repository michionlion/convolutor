package framework.commands;

public class HelpCommand extends Command {
    
    protected HelpCommand() { // protect against initilization outside of command package
        
    }
    
    @Override
    public String getCommand() {
        return "help";
    }
    
    @Override
    public boolean isAlias(String s) {
        return s.equalsIgnoreCase("whatis") || s.equalsIgnoreCase("what");
    }
    
    @Override
    public String invoke(String... args) {
        if(args.length<2) {
            StringBuilder cs = new StringBuilder();
            
            for(Command s : cmds) {
                cs.append(cs.length()!=0 ? "\n|  " : "List of commands:").append(s.getCommand());//append(" - ").append(s.helpString()); // first slot is empty command, so ensure that it doesn't have '|  ' in front
            }
            cs.append("\nUse 'help <command>' to learn more about a specific command!");
            return cs.toString();
        }
        
        return getCommandFromString(args[1]).helpString();
    }
    
    @Override
    public String helpString() {
        return "Command for helping with commands!\nUseage:\nhelp <command> - returns the help string of command";
    }
    
}
