package framework.commands;

import framework.operations.IO;
import framework.operations.Status;

public class SaveCommand extends Command {

    protected SaveCommand() {
        
    }
    
    @Override
    public boolean isAlias(String s) {
        return s.equals("s");
    }
    
    @Override
    public String getCommand() {
        return "save";
    }

    @Override
    public String invoke(String... args) { // cannot save and directly overwrite via terminal, must use GUI for that feature (alone)
        Status outcome = IO.save(args.length>1 ? args[1] : null); // param will be null if args not specified, so will ask for name
        return !outcome.isError() ? "Saved as "+ IO.getRecentModifiedFile().getName() : "ERROR: "+ outcome.getMsg();
    }

    @Override
    public String helpString() {
        return "Saves the current working image\nUseage:\nsave\nsave <filename>";
    }
    
}
