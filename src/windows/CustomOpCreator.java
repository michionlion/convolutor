package windows;

import framework.operations.IO;
import framework.operations.Status;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dialog;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.KeyboardFocusManager;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusAdapter;
import java.awt.event.FocusEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.text.ParseException;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JFormattedTextField.AbstractFormatter;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import static windows.CustomOpCreator.CUSTOM_OP_MODE.*;

/**
 *
 * CustomOpCreator is a 'static' class that can be used to get an Op created by
 * the user through a graphical interface. A simple call to getOp() will block
 * until the user has created an Op, and then return said Op CustomOpCreator is
 * quite shoddily organized, with many, many inner classes and anonymous
 * listeners, though it does accomplish its task.
 *
 * @author Michionlion | Saejin Mahlau-Heinert
 */
public abstract class CustomOpCreator {

    private static final Object pollObject = new Object(); // object to use for notify and wait calls

    private static JDialog dialog;

    private static String name = "name"; // values to use for creation of op, gets set by optionPane's actionlisteners
    private static CUSTOM_OP_MODE type = COMBINATION_OP;
    private static int[] mtrxSize = {3, 3};
    private static boolean normalized = true;
    private static boolean grayscaled = false;
    private static boolean simultaneous = false;
    private static boolean successful = false; // set when confirm is hit, if not true then don't use values above

    // init these after above values so that correct defaults get used
    private static InputPane inputPane = new ComboPane();
    private static OptionPane optionPane = new OptionPane(COMBINATION_OP);

    public enum CUSTOM_OP_MODE {

        COMBINATION_OP("combo"), MATRIX_OP("matrix");

        private final String saveName; // text to put in save files to signify type

        CUSTOM_OP_MODE(String saveName) {
            this.saveName = saveName;
        }

        public String getSaveName() {
            return saveName;
        }
    }

    private static interface InputPane {

        public JComponent getPanel();
    }

    private static void changeMode(CUSTOM_OP_MODE m) {
        type = m;
        optionPane.createGUI(type);
        inputPane = null;
        switch (type) {
            case COMBINATION_OP:
                inputPane = new ComboPane();
                break;
            case MATRIX_OP:
                inputPane = new MatrixPane(mtrxSize[0], mtrxSize[1]);
                break;
            default:
                return; //HOW?
        }

        createGUI();
    }

    private static void createGUI() {
        dialog.getContentPane().removeAll();

        dialog.getContentPane().setLayout(new BorderLayout());
        dialog.getContentPane().add(inputPane.getPanel(), BorderLayout.CENTER);
        dialog.getContentPane().add(optionPane, BorderLayout.EAST);

        dialog.revalidate();
        dialog.pack();

        dialog.repaint();
    }

    /**
     * getOp will return null if the user cancels or closes the dialog!
     *
     * @param parent the frame to parent this dialog to, or null
     * @param flags an optional list of flags that can define some behavior. If
     * flags is not empty, this method will print feedback to the terminal
     * @return an description of what happened
     */
    public static Status getOp(Frame parent, Object... flags) {

        successful = false;

        dialog = new JDialog(parent);
        dialog.setModalityType(Dialog.ModalityType.APPLICATION_MODAL);
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        dialog.setLocationByPlatform(true);
        
        dialog.addWindowListener(new WindowAdapter() { // call notifyOpDone() when window is closed, to dispose of resources and return to getOp()
            @Override
            public void windowClosed(WindowEvent e) {
                notifyOpDone(true); // coming from closing window, so passing true
            }
        });

        inputPane = new ComboPane(); // reset GUI elements to avoid carrying over anything from last time
        optionPane = new OptionPane(type);

        createGUI();

        dialog.setVisible(true); // starts blocking here, until notifyOpDone() is called

        StringBuilder result = new StringBuilder();

        if (successful) {
            result.append(name).append(";").append(type.getSaveName()).append("\n");
            switch (type) {
                case COMBINATION_OP:
                    result.append("Simultaneous:").append(simultaneous);
                    String fs = inputPane.toString();
                    result.append(fs.length() > 0 ? "\n" : "").append(fs);
                    break;
                case MATRIX_OP:
                    result.append(mtrxSize[0]).append("x").append(mtrxSize[1]).append("\n");
                    result.append("Normalized:").append(normalized).append("\n");
                    result.append("Grayscaled:").append(grayscaled).append("\n");
                    result.append(inputPane.toString());
                    break;
            }
        } else {
            if (flags.length > 0) {
                Terminal.addCommandOutput("create", Status.CANCELED_OPERATION.getMsg());
            }
            return Status.CANCELED_OPERATION;
        }

        dialog.dispose(); // finish resources and close everything
        dialog = null;
        
        String info = "Created " + name;
        Status outcome = IO.saveOp(result.toString());
        if (outcome.isError()) {
            info = "Error when saving the Op: " + outcome.getMsg();
        }
        
        
        try { // load op to add to list, ensure format is correct (which it should always be)
            IO.loadOp(name);
        } catch (IO.OperationFormatWrong ex) {
            info = info.concat("ERROR! Save format for operation was incorrect, please try again"); // should never get here
        }

        

        if (flags.length > 0) {
            Terminal.addCommandOutput("create", info);
        }

        
        if(outcome.isError()) return outcome;
        return Status.SUCCESSFUL_OPERATION;
    }

    private static void notifyOpDone(boolean closed) {
        if (!closed && inputPane.toString() == null) { // error msg was shown, user must do something, or another error may have happened.  Reguardless, don't close window
            successful = false; // make sure this is reset, if this method was called by confirm button this could be true
            return;
        }
        if (dialog != null && dialog.isDisplayable()) {
            dialog.setVisible(false); // return to getOp's setVisible(true) call, let execution continue
        }        //other disposals/visibility changes, if needed
    }

    public static class MatrixPane extends JPanel implements InputPane {

        private final JFormattedTextField[][] boxGrid;
        private final int width, height;

        public MatrixPane(int width, int height) {
            this.width = width;
            this.height = height;
            boxGrid = new JFormattedTextField[width][height];

            setLayout(new GridLayout(width, height));

            //set up 2D array and add elements to this panel
            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    JFormattedTextField box = new JFormattedTextField(new MatrixFormatter());
                    box.setMinimumSize(new Dimension(12 * 3, 12));
                    box.setAlignmentX(Component.CENTER_ALIGNMENT);
                    box.setAlignmentY(Component.CENTER_ALIGNMENT);
                    box.setFocusLostBehavior(JFormattedTextField.COMMIT_OR_REVERT);
                    box.setValue(0);
                    box.setColumns(4);
                    boxGrid[x][y] = box;
                    add(boxGrid[x][y]);
                }
            }

        }

        public void showError(Exception ex) {
            JOptionPane.showMessageDialog(dialog, "Error in matrix panel: " + ex.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }

        @Override
        public JComponent getPanel() {
            return this;
        }

        @Override
        public String toString() {
            float[][] res = new float[width][height]; // taking this extra step instead of going directly to stringbuilder to verify data

            for (int x = 0; x < width; x++) {
                for (int y = 0; y < height; y++) {
                    try {
                        boxGrid[x][y].commitEdit();
                    } catch (ParseException ex) { // shouldn't really ever happen, unless user is editing box and somehow clicks confirm without switching focus
                        showError(ex);
                        res[x][y] = Float.NaN; // contingency if we get past showError and the user has not corrected values
                        continue;
                    }
                    Object val = boxGrid[x][y].getValue();
                    res[x][y] = ((val instanceof Float) ? (float) val : Float.NaN);
                }
            }

            StringBuilder result = new StringBuilder(width * height * 2); // estimate size

            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    result.append(res[x][y]).append(x < width - 1 ? " " : ""); // don't print space on last num
                }
                result.append(y < height - 1 ? "\n" : ""); // newline after row
            }

            return result.toString();
        }

        public int[] getMatrixSize() {
            int[] res = {width, height};
            return res;
        }

        public class MatrixFormatter extends JFormattedTextField.AbstractFormatter { // fancy formatter for the text fields that will round a number to zero and not display 0.0 but instead just 0

            public static final float CUT_OFF_ROUND = 0.000001f;

            @Override
            public Object stringToValue(String text) throws ParseException {
                try {
                    return Float.parseFloat(text);
                } catch (Exception ex) {
                    throw new ParseException(text + " cannot be parsed", 0);
                }
            }

            @Override
            public String valueToString(Object value) throws ParseException {
                float val;
                try {
                    val = Float.parseFloat(value.toString());
                } catch (Exception ex) {
                    throw new ParseException(value + " cannot be parsed!", 0);
                }
                //deal with floating-point errors, round to int if fits
                if (Math.abs(Math.round(val) - val) < CUT_OFF_ROUND) {
                    return Integer.toString(Math.round(val));
                } else {
                    return Float.toString(val);
                }
            }
        }
    }

    public static class ComboPane extends JPanel implements InputPane {

        JTextArea area;
        JScrollPane pane;

        private final Highlighter.HighlightPainter errorPainter;

        public ComboPane() {
            area = new JTextArea("Enter operation names here,\neach operation on a line", 6, 16);
            area.getDocument().addDocumentListener(new ValidFileChecker());

            setLayout(new BorderLayout(5, 5)); // borderlayout with 5 pix gap

            pane = new JScrollPane(area, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);

            errorPainter = new DefaultHighlighter.DefaultHighlightPainter(Color.RED);

            add(pane);
        }

        @Override
        public JComponent getPanel() {
            return this;
        }

        @Override
        public String toString() {

            StringBuilder res;
            try (Scanner txt = new Scanner(area.getText())) { // try with resources to ensurer if any errors occur, txt is closed
                res = new StringBuilder(); // estimate length (not needed)
                while (txt.hasNext()) {
                    String s = txt.nextLine();
                    if (s == null || s.equalsIgnoreCase("")) { // error checks for empty and/or invalid lines
                        continue;
                    } else if (!fileValid(s)) {
                        int choice = JOptionPane.showConfirmDialog(dialog, "<html>'"+s + "' is not an op file, do you wish to continue?<br>'" + s + "' will be removed if so.</html>", "", JOptionPane.YES_NO_OPTION, JOptionPane.ERROR_MESSAGE);
                        if (choice == JOptionPane.NO_OPTION) {
                            return null;
                        }
                        //otherwise, replace s with nothing and continue
                        area.setText(area.getText().replace(s, ""));
                        continue;
                    }
                    
                    res.append(res.length() == 0 ? "" : "\n").append(s); // if s isn't empty, append it plus a newline if needed
                }
            }

            return res.toString();
        }

        private boolean validateFiles() { // ensure files listed exist and are readable, and highlight anything wrong in red
            area.getHighlighter().removeAllHighlights(); // clear highlights on every change so that we can parse again

            String text = area.getText();
            String textL = text.toLowerCase(); // don't call toLowerCase() a whole bunch, saves on memory (toLowerCase creates new strings every time it is called, adding to the internal pool

            boolean allValid = true;

            int checkPos = 0;
            int endPos = 0;
            String eval;
            while (endPos < text.length()) {
                char endChar = textL.charAt(endPos);
                if (endChar == '\n' || endChar == '.') { // test if next char is newline or ., if so we've reached end of this filename
                    eval = text.substring(checkPos, endPos);
                    if (!fileValid(eval)) { // if eval is not a valid file

                        allValid = false; // we have at least one error
                        try {
                            area.getHighlighter().addHighlight(checkPos, endPos, errorPainter);
                        } catch (BadLocationException ex) {
                            Logger.getLogger(CustomOpCreator.class.getName()).log(Level.SEVERE, null, ex); //stuff went wrong, log it
                        }
                    } else if (eval.equalsIgnoreCase(".op") || eval.equalsIgnoreCase(".")) {
                        try {
                            area.getHighlighter().addHighlight(checkPos - 1, endPos, errorPainter);
                        } catch (BadLocationException ex) {
                            Logger.getLogger(CustomOpCreator.class.getName()).log(Level.SEVERE, null, ex); //stuff went wrong, log it
                        }
                    }

                    checkPos = endChar == '\n' ? endPos + 1 : endPos; // do +1 to skip over new line or . that was sensed
                }
                endPos++;

            }

            return allValid;
        }

        public static boolean fileValid(String path) {
            if (path.equalsIgnoreCase(".op") || path.equalsIgnoreCase("\n") || path.equalsIgnoreCase(".")) {
                return false; // sanity checks for other possible files that may create problems
            }
            File f = new File("operations" + File.separator + path + ".op");
            return f.isFile() && f.canRead();
        }

        public class ValidFileChecker implements DocumentListener { // just calls validateText() when changes occur

            @Override
            public void insertUpdate(DocumentEvent e) {
                validateFiles();
            }

            @Override
            public void removeUpdate(DocumentEvent e) {
                validateFiles();
            }

            @Override
            public void changedUpdate(DocumentEvent e) {
                validateFiles();
            }

        }

    }

    public static final class OptionPane extends JPanel {

        //options for all ops
        private JTextField name;
        private JComboBox<CUSTOM_OP_MODE> type; // specify combo box is using CUSTOM_OP_MODEs as values
        private JButton confirm;

        //option pane to create/switch for diff types of ops, set with actionlisteners in createOptions
        private JPanel options;
        private JPanel top;

        public OptionPane(CUSTOM_OP_MODE defaultSelectionMode) {
            createGUI(defaultSelectionMode);
        }

        public void createGUI(CUSTOM_OP_MODE mode) {
            removeAll();

            setLayout(new BorderLayout()); // use borderlayout so that whitespace expands between confirm button and rest of top panel

            top = new JPanel();
            top.setLayout(new BoxLayout(top, BoxLayout.Y_AXIS));

            JLabel l = new JLabel("-Parameters-");
            l.setAlignmentX(JComponent.CENTER_ALIGNMENT);
            top.add(l);
            // create name and type, then add to panel
            name = new JTextField(CustomOpCreator.name, 12);
            name.setText(CustomOpCreator.name);
            name.setToolTipText("Operation name");
            name.getDocument().addDocumentListener(new DocumentListener() { // add a listener to ensure that any change to the text is reflected by the name variable in CustomOpCreator

                @Override
                public void insertUpdate(DocumentEvent e) {
                    CustomOpCreator.name = name.getText();
                }

                @Override
                public void removeUpdate(DocumentEvent e) {
                    CustomOpCreator.name = name.getText();
                }

                @Override
                public void changedUpdate(DocumentEvent e) {
                    CustomOpCreator.name = name.getText();
                }
            });

            Object[] box = {MATRIX_OP, COMBINATION_OP}; // combobox contains the two types possible
            type = new JComboBox(box);
            type.setEditable(false);
            type.setToolTipText("Operation type");
            type.setSelectedItem(mode); // select the current mode so we don't get callbacks and recreate this unnecessarily
            type.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    Object val = ((JComboBox) e.getSource()).getSelectedItem();
                    changeMode((CUSTOM_OP_MODE) val); // change mode
                }
            });

            top.add(name);
            top.add(type);

            // create options pane and add (thing that sets normalization, simultaneous, grayscale, size, etc, with actionlisteners to update relevent instance vars
            options = new JPanel();
            options.setLayout(new BoxLayout(options, BoxLayout.Y_AXIS)); // can't do this in constructor of options, since options isn't initialized then, and null gets passed

            if (mode == COMBINATION_OP) {

                JCheckBox simul = new JCheckBox("Simultaneous", CustomOpCreator.simultaneous); // simultaneous checkbox
                simul.setToolTipText("If the operations should execute together or sequentially");
                simul.setAlignmentX(JComponent.CENTER_ALIGNMENT);
                simul.addItemListener(new ItemListener() {

                    @Override
                    public void itemStateChanged(ItemEvent e) {
                        simultaneous = e.getStateChange() == ItemEvent.SELECTED; // get and then set state of simultaneous
                    }
                });

                options.add(simul);

            } else if (mode == MATRIX_OP) {

                JFormattedTextField size = new JFormattedTextField(new SizeFormatter()); // formatted text field to ensure entered values are valid
                size.setValue(CustomOpCreator.mtrxSize); // set initial value
                size.setToolTipText("Size of the matrix, (width)x(height)");
                size.setAlignmentX(JComponent.CENTER_ALIGNMENT);

                // ensure changes are made to inputPane, can't use document listener since it may not work as expected with a formatted field
                size.addFocusListener(new FocusAdapter() {
                    @Override
                    public void focusLost(FocusEvent e) {
                        mtrxSize = (int[]) ((JFormattedTextField) e.getSource()).getValue();
                        inputPane = new MatrixPane(mtrxSize[0], mtrxSize[1]);
                        CustomOpCreator.createGUI(); // reset GUI so that new InputPane is shown
                    }
                });
                size.addActionListener(new ActionListener() { // actionlister to listen for ENTER or equivelent keypress and remove focus
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        JFormattedTextField s = (JFormattedTextField) e.getSource();
                        KeyboardFocusManager.getCurrentKeyboardFocusManager().focusNextComponent(); // focuses next component, so that focusLost is called and the user can no longer type in the text field
                    }
                });

                JCheckBox norm = new JCheckBox("Normalized", CustomOpCreator.normalized);
                norm.setToolTipText("If the matrix should be normalized");
                norm.setAlignmentX(JComponent.CENTER_ALIGNMENT);
                norm.addItemListener(new ItemListener() {

                    @Override
                    public void itemStateChanged(ItemEvent e) {
                        normalized = e.getStateChange() == ItemEvent.SELECTED; // get and then set state of simultaneous
                    }
                });

                JCheckBox gray = new JCheckBox("Grayscaled", CustomOpCreator.grayscaled);
                gray.setToolTipText("If the operation should be applied only on a grayscaled image");
                gray.setAlignmentX(JComponent.CENTER_ALIGNMENT);
                gray.addItemListener(new ItemListener() {

                    @Override
                    public void itemStateChanged(ItemEvent e) {
                        grayscaled = e.getStateChange() == ItemEvent.SELECTED; // get and then set state of simultaneous
                    }
                });

                options.add(size);
                options.add(norm);
                options.add(gray);

            } else {
                options.add(new JLabel("NOT A RECOGNIZED MODE")); // shouldn't ever happen
            }

            top.add(options);

            add(top, BorderLayout.NORTH);

            confirm = new JButton("Create");
            confirm.addActionListener(new ActionListener() {
                @Override
                public void actionPerformed(ActionEvent e) {
                    successful = true;
                    CustomOpCreator.notifyOpDone(false);
                }
            });

            add(confirm, BorderLayout.SOUTH);

            validate(); // ensure removeAll() went smoothly
            repaint();
        }

        public class SizeFormatter extends AbstractFormatter { // formatter that will ensure entered values are correct

            @Override
            public Object stringToValue(String text) throws ParseException {
                int[] val = new int[2]; // value is an int array of length 2

                String[] size;
                if (text.contains("x")) {
                    size = text.split("x");
                } else if (text.contains("X")) {
                    size = text.split("X");
                } else {
                    throw new ParseException(text + " is not parseable!", 0);
                }

                if (size.length != 2) {
                    throw new ParseException(text + " is not parseable!", 0);
                }

                try {
                    val[0] = Integer.parseInt(size[0]);
                } catch (Exception ex) {
                    throw new ParseException(size[0] + " is not parseable!", 0);
                }
                try {
                    val[1] = Integer.parseInt(size[1]);
                } catch (Exception ex) {
                    throw new ParseException(size[1] + " is not parseable!", 0);
                }

                if (val[0] % 2 == 0) {
                    throw new ParseException(val[0] + " is not odd!", 0);
                }
                if (val[1] % 2 == 0) {
                    throw new ParseException(val[1] + " is not odd!", 2);
                }

                return val;
            }

            @Override
            public String valueToString(Object value) throws ParseException {
                int[] size;
                try {
                    size = (int[]) value;
                } catch (Exception ex) {
                    throw new ParseException(value + " is not parseable!", 0);
                }

                if (size != null && size.length != 2) {
                    throw new ParseException(value + " is not parseable!", 0);
                }

                return size == null ? "" : size[0] + "x" + size[1];
            }

        }

    }

}