package windows;

import framework.commands.Command;
import framework.operations.IO;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Frame;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.SwingUtilities;

public class Terminal extends JFrame {

    public static final int T_WIDTH = 450, T_HEIGHT = 300;
    public static final String GREET_TEXT = "Welcome to Convoluter!\nFor command line help, type 'help' in the text box below.\n";

    private static Terminal currentInstance = null;

    private JTextField input;
    private JTextArea output;
    private JScrollPane outputPane;

    private JMenuBar menuBar; // menubar
    private JMenu file;
    private JMenuItem save;
    private JMenuItem saveAs;
    private JMenuItem load;

    private Terminal() { // constructor
        super("Terminal");
        setSize(T_WIDTH, T_HEIGHT);
        setMinimumSize(new Dimension(T_WIDTH, T_HEIGHT));
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
//        setAlwaysOnTop(true);
        setLocationByPlatform(true);

        // set up input text box
        input = new JTextField(T_WIDTH / 8); // 8 is avg width of char 
        input.setEditable(true);
        input.setMaximumSize(new Dimension(Integer.MAX_VALUE, 12));
        input.setMinimumSize(new Dimension(T_WIDTH, 12));
        input.addActionListener(new CommandListener()); // add command listener to trigger process()

        //set up output area
        output = new JTextArea(GREET_TEXT, T_HEIGHT / 12 - 24, T_WIDTH / 8);
        output.setEditable(false);
        output.setSize(T_WIDTH, T_HEIGHT - 24);
        output.setToolTipText(null);
        output.setLineWrap(true);
        output.setAutoscrolls(true);
        output.setWrapStyleWord(true);

        //set up scroll area
        outputPane = new JScrollPane(output, JScrollPane.VERTICAL_SCROLLBAR_ALWAYS, JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
        outputPane.setAutoscrolls(true);

        menuBar = new JMenuBar();

        file = new JMenu("File");

        save = new JMenuItem("Save", KeyEvent.VK_S);
//        save.addActionListener((ActionEvent e) -> { // lambda, equivlent to creating an anonymous class implementing ActionListener with the contents of actionPerformed being the next line
//            Terminal.processGUI("save " + IO.getRecentModifiedFile().getName());
//        }); // had to disable because java 8 is not on alden computers, and can't install it
        
        save.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Terminal.processGUI("save " + IO.getRecentModifiedFile().getName());
            }
        });

        saveAs = new JMenuItem("Save As", KeyEvent.VK_S);
        saveAs.setAccelerator(KeyStroke.getKeyStroke('s', ActionEvent.CTRL_MASK));
//        saveAs.addActionListener((ActionEvent e) -> { // lambda again.  SO COOL
//            Terminal.processGUI("save");
//        });
        
        saveAs.setAccelerator(KeyStroke.getKeyStroke('s', ActionEvent.CTRL_MASK));
        saveAs.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent a) {
                Terminal.processGUI("save");
            }
        });

        load = new JMenuItem("Load Image", KeyEvent.VK_L);
//        load.addActionListener((ActionEvent e) -> { // lambda again.
//            Terminal.processGUI("use");
//        });
        
        load.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent a) {
                Terminal.processGUI("use");
            }
        });

        file.add(save);
        file.add(saveAs);
        file.addSeparator();
        file.add(load);

        menuBar.add(file);

        setJMenuBar(menuBar);

        // ensure if window is focused, focus goes to input box
        addWindowFocusListener(new WindowAdapter() { 
            @Override
            public void windowGainedFocus(WindowEvent e) {
                focus();
            }
        });
        
        output.addFocusListener(new FocusListener() {

            @Override
            public void focusGained(FocusEvent e) {
                focus();
            }

            @Override
            public void focusLost(FocusEvent e) {
                //do nothing
            }
        });

        // add components to jframe's content pane
        getContentPane().setLayout(new BoxLayout(getContentPane(), BoxLayout.Y_AXIS)); // cannot use JFrame, must use contentPane for reasons unknown
        getContentPane().add(outputPane);
        getContentPane().add(input);
        
        setVisible(true);
    }

    public void process(String cmd) {
        if (cmd == null || cmd.trim().equalsIgnoreCase("")) {
            return;
        }
        cmd = cmd.trim();
        String[] vals = cmd.split(" "); //assuming arguments are separated by spaces

        addCommandOutput(cmd, Command.getCommandFromString(vals[0]).invoke(vals));

        validate(); // must use before getMaximum so that we use the current value after the text was output above
        JScrollBar b = outputPane.getVerticalScrollBar();
        b.setValue(b.getMaximum()); // scroll to the bottom
    }

    private void out(String s) {
        output.append(s);
    }

    private void outln(String s) {
        out(s + "\n");
    }

    public static void processGUI(String commandString) { // just a util method for GUI classes to use when directly accessing terminal commands (such as save)
        if(currentInstance!=null) currentInstance.process(commandString);
        else { // smaller, no terminal option mode
            String[] vals = commandString.trim().split(" ");
            Command.getCommandFromString(vals[0]).invoke(vals);
        }
    }

    public static void addCommandOutput(String operation, String output) { // operation should be the command executed (the result of getCommand() on the invoked command, output is the result of invoke()
        if(currentInstance!=null) currentInstance.outln(">" + operation + "\n" + output);
    }

    public static void focus() {
        if(currentInstance==null) return; // avoid nullpointer
        currentInstance.toFront();
        currentInstance.requestFocus();
        currentInstance.input.requestFocusInWindow();
        
    }

    public static void openTerminal() {
        currentInstance = new Terminal();
    }
    
    public static Frame getFrameToChild() { // bad form to export the private singleton, but not really another simple way to parent things to it in other classes
        Frame f;
        if(currentInstance!=null) return currentInstance;
        else if(Display.getFrameToChild()!=null) return Display.getFrameToChild();
        else return null;
    }

    public static void main(final String[] args) { // did not know you could set a param as final.  Huh. Useful.
        SwingUtilities.invokeLater(new Runnable() { // do UI-related things in Swing's EDT

            @Override
            public void run() {
                if(args.length>0) {
                    if(args[0].equalsIgnoreCase("terminal")) {
                        Terminal.openTerminal();
                    } else if(args[0].equalsIgnoreCase("display")) {
                        Display.openDisplay();
                        System.out.println("Opening in display only mode!");
                    } else { // only filename specified
                        Terminal.openTerminal();
                        Display.openDisplay();
                        Terminal.processGUI("use "+args[0]);
                    }
                    
                    if(args.length>1) Terminal.processGUI("use "+args[1]); // if mode was specified then starting file load that
                    
                    
                } else {
                    Terminal.openTerminal();
                    Display.openDisplay();
                }
            }
        });
    }

    private class CommandListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            process(input.getText());
            input.setText(null);
        }

    }
}
