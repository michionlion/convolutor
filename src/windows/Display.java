package windows;

import framework.operations.OPS;
import framework.operations.Status;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Frame;
import java.awt.Graphics2D;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseWheelEvent;
import java.awt.image.BufferedImage;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSplitPane;
import javax.swing.SwingConstants;

public class Display extends JFrame {

    private static Display currentInstance = null; // used for singleton-esk operation

    private Toolbar toolbar;
    private JLabel img;
    private JScrollPane imageContent;
    
    private JSplitPane content;
    
    private float zoomLevel = 1;
    
//      this strategy doesn't work to fix the bug with big images perhaps?  It's still around for default tiger.png, must move frame or resize to get the image to update, super weird
//    Timer timedRefresh = new Timer(1000, new ActionListener() { // refreshes image every second to ensure changes do not go undetected (sometimes repaint(), when called once, will not use most recent info, possibly missing a validate() call somewhere
//
//        @Override
//        public void actionPerformed(ActionEvent e) {
//            repaint();
//        }
//    });
    

    private Display() {
        super("Convolutor");
        setLocationByPlatform(true);
        setDefaultCloseOperation(JDialog.EXIT_ON_CLOSE);
        setMaximumSize(Toolkit.getDefaultToolkit().getScreenSize()); // may have problems with multi-monitor systems, also doesn't seem to work, period
        
        content = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, true);
        content.setDividerSize(2);
        content.setEnabled(false); // don't allow resizing ... purely a design choice, makes the JSplitPane practically useless anyways, but always the option to reenable
        
        img = new JLabel(new ImageIcon(OPS.getCurrentImage()));
        imageContent = new JScrollPane(img, JScrollPane.VERTICAL_SCROLLBAR_AS_NEEDED, JScrollPane.HORIZONTAL_SCROLLBAR_AS_NEEDED);
        imageContent.setWheelScrollingEnabled(false);
        
//        getContentPane().add(imageContent);
        content.setLeftComponent(imageContent);
        
        toolbar = new Toolbar();
//        getContentPane().add(toolbar);
        content.setRightComponent(toolbar);
        
        content.setResizeWeight(1);
        
        add(content);
        
        addMouseWheelListener(new MouseAdapter() {

            @Override
            public void mouseWheelMoved(MouseWheelEvent e) {
                double s = e.getPreciseWheelRotation(); // finishes positive if 'up' or 'in'
                
                zoomLevel-=s/10f;
                if(zoomLevel>5f) zoomLevel = 5f;
                else if(zoomLevel<0.1f) zoomLevel = 0.1f;
                
                zoomLevel = Math.round(zoomLevel*100)/100f; // fix any floating-point errors
                img.setIcon(getZoomed());
                repaint();
            }
        });
        
        
        
        pack();
        setVisible(true);
    }
    
    public ImageIcon getZoomed() {
        BufferedImage curr = OPS.getCurrentImage();
        BufferedImage zoomed = new BufferedImage(Math.round(curr.getWidth()*zoomLevel), Math.round(curr.getHeight()*zoomLevel), BufferedImage.TYPE_INT_ARGB);
        
        // scale curr to fit in zoomed, using this instead of getScaledInstance for speed
        Graphics2D g = (Graphics2D) zoomed.getGraphics();
        g.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g.drawImage(curr, 0, 0, zoomed.getWidth(), zoomed.getHeight(), null);
        g.dispose();
        
        toolbar.ZOOM.setText("Zoom: "+zoomLevel);
        
        return new ImageIcon(zoomed);
    }
    
    public static Frame getFrameToChild() {
        Frame f;
        if(currentInstance!=null) return currentInstance;
        else if(Terminal.getFrameToChild()!=null) return Terminal.getFrameToChild();
        else return null;
    }

    public static void openDisplay() {
        currentInstance = new Display();
        Terminal.focus();
    }
    
    public static void refreshDisplay() { // trigger refresh after op, also using timed refresh
        if(currentInstance==null) return;
        currentInstance.img.setIcon(currentInstance.getZoomed());
        currentInstance.img.revalidate(); // individual revalidate() calls to ensure they happen, weird bugs around these, perhaps using them incorrectly?
        currentInstance.imageContent.revalidate();
        currentInstance.revalidate();
        currentInstance.pack();
        
        // clip sizes (max size doesn't work for some reason)
        Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension curr = currentInstance.getSize();
        
        if(curr.getWidth()>screen.getWidth()) { // clip width
            currentInstance.setExtendedState(JFrame.MAXIMIZED_BOTH);
            Terminal.focus();
        }
        
        if(curr.getHeight()>screen.getHeight()) { // clip height
            currentInstance.setExtendedState(JFrame.MAXIMIZED_BOTH);
            Terminal.focus();
        }
        
        
        
        
        currentInstance.repaint();
    }
    
    private static class Tool implements ActionListener { // holds relevent info for one command, and is also the actionlistener for that command's button
        String cmd;
        String name;
        ImageIcon icon;
        
        public Tool(String cmd) {
            this.cmd = cmd;
            name = null;
            icon = null;
        }
        public Tool(String name, String cmd) {
            this.name = name;
            this.cmd = cmd;
            icon = null;
        }
        public Tool(String name, ImageIcon icon, String cmd) {
            this.name = name;
            this.icon = icon;
            this.cmd = cmd;
        }
    
        @Override
        public void actionPerformed(ActionEvent e) {
            Terminal.processGUI(cmd);
        }
    }

    private static final Tool[] cmds = {new Tool("GaussBlur",new ImageIcon("icons/gaussBlur.png"),"apply gaussBlur"),new Tool("BoxBlur",new ImageIcon("icons/boxBlur.png"),"apply boxBlur"),
        new Tool("Sobel",new ImageIcon("icons/sobel.png"),"apply sobel"),new Tool("SimpleEdge",new ImageIcon("icons/simple-edge.png"),"apply simple-edge"),
        new Tool("Sharpen",new ImageIcon("icons/sharpen.png"),"apply sharpen"),new Tool("Unsharpen",new ImageIcon("icons/unsharpen.png"),"apply unsharpen")};
    
    public class Toolbar extends JPanel {
        public static final int BUTTON_MINIMUM=32;
        
        private final JPanel tools;
        
        private final JPanel upperContent;
        private final JButton custom;
        private final JButton create;
        private final JButton load;
        private final JButton save;
        
        public final JButton ZOOM;
        
        public Toolbar() {
            
            // new GridLayout(cmds.length%2==0 ? cmds.length/2 : cmds.length/2 + 1, 2) // old layout
            
            tools = new JPanel(new GridBagLayout());
            tools.setMinimumSize(new Dimension(BUTTON_MINIMUM*2, BUTTON_MINIMUM*cmds.length/2));
            tools.setPreferredSize(new Dimension(BUTTON_MINIMUM*2, BUTTON_MINIMUM*cmds.length/2));
            
            int gridx = 0;
            GridBagConstraints c = new GridBagConstraints();
            c.fill = GridBagConstraints.NONE; // constraints for all buttons
            c.anchor = GridBagConstraints.CENTER;
            
            for (Tool t : cmds) { // add buttons for each command
                JButton button = t.icon!=null ? new JButton(t.icon) : new JButton(t.name); // use icon if it was set, otherwise use name
                button.addActionListener(t);
                button.setPreferredSize(new Dimension(BUTTON_MINIMUM, BUTTON_MINIMUM));
                
                
                c.gridx = gridx%2==0 ? 0 : 1; // individual constraints
                c.gridy = (int)(gridx/2);
                
                tools.add(button, c);
                
                gridx++;
            }
            
            c.gridx = 0;
            c.gridy = 0;
            c.fill = GridBagConstraints.HORIZONTAL;
            
            upperContent = new JPanel();
            upperContent.setLayout(new GridBagLayout());
            
            upperContent.add(tools, c);
            
            custom = new JButton("Apply Op");
            custom.setMargin(new Insets(0, 0, 0, 0));
            custom.setFont(new Font("Ariel", Font.PLAIN, 11));
            custom.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    
                    String name = JOptionPane.showInputDialog(null, "Operation Name?");
                    if(name==null || name.equals("")) return;
                    Terminal.processGUI("apply " + name);
                    Terminal.focus();
                }
            });
            c.gridy = 1;
            upperContent.add(custom, c);
            
            create = new JButton("Create Op");
            create.setMargin(new Insets(0, 0, 0, 0));
            create.setFont(new Font("Ariel", Font.PLAIN, 11));
            create.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    Status s = CustomOpCreator.getOp(Display.getFrameToChild(), true);
                    if(s.isError() && s!=Status.CANCELED_OPERATION) JOptionPane.showMessageDialog(Display.getFrameToChild(), s.getMsg(), "ERROR", JOptionPane.ERROR_MESSAGE); // something went wrong, tell the user
                }
            });
            c.gridy = 2;
            upperContent.add(create, c);
            
            
            load = new JButton("Load");
            load.setMargin(new Insets(0, 0, 0, 0));
            load.setFont(new Font("Ariel", Font.PLAIN, 11));
            load.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    Terminal.processGUI("load");
                    Terminal.focus();
                }
            });
            c.gridy = 3;
            upperContent.add(load, c);
            
            
            save = new JButton("Save");
            save.setMargin(new Insets(0, 0, 0, 0));
            save.setFont(new Font("Ariel", Font.PLAIN, 11));
            save.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    Terminal.processGUI("save");
                    Terminal.focus();
                }
            });
            c.gridy = 4;
            upperContent.add(save, c);
            
            
            
            
            
            setLayout(new BorderLayout());
            setPreferredSize(new Dimension(BUTTON_MINIMUM*2, 0)); // need both for some reason to set preferred size that is used
            setMinimumSize(new Dimension(BUTTON_MINIMUM*2, 0));
            add(upperContent, BorderLayout.NORTH);
            
            
            
            ZOOM = new JButton("Zoom: 1.0");
            ZOOM.setMargin(new Insets(0, 0, 0, 0));
            ZOOM.setFont(new Font("Ariel", Font.BOLD, 11));
            ZOOM.setHorizontalAlignment(SwingConstants.CENTER);
            ZOOM.setBorder(null);
            ZOOM.setAlignmentX(JComponent.CENTER_ALIGNMENT);
            
            ZOOM.addActionListener(new ActionListener() {

                @Override
                public void actionPerformed(ActionEvent e) {
                    String in = JOptionPane.showInputDialog("Set zoom level:");
                    try {
                        zoomLevel = Float.parseFloat(in);
                        if(zoomLevel<0.01) zoomLevel = 0.01f;
                        refreshDisplay();
                    } catch(Exception ex) {
                        // just return
                    }
                }
            });
            
            add(ZOOM, BorderLayout.SOUTH);
            pack();
        }
    }
}
